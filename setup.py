from setuptools import setup, find_packages

setup(
    name='model-carbon-intensity',
    version='0.1',
    description='models for carbon intensity',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)